package dbNTNU;

import java.sql.*;
import java.util.Properties;

public abstract class DBConnector {

    protected Connection conn;
    private static String dbhost = "jdbc:mysql://localhost:3306";
    private static String username = "root";
    private static String password = "1234";


    public DBConnector () {
    }
    public void connect() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            // Properties for user and password.
            Properties p = new Properties();
            p.put("user", username);
            p.put("password", password);
            //            conn = DriverManager.getConnection("jdbc:mysql://mysql.ansatt.ntnu.no/sveinbra_ektdb?autoReconnect=true&useSSL=false",p);
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/piazzadb",p);
        } catch (Exception e)
        {
            throw new RuntimeException("Unable to connect", e);
        }
    }



}
